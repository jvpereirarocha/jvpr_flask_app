from flask import Blueprint, render_template
from flask_login import login_user


usr = Blueprint('user', __name__, url_prefix='/user')


@usr.route('/login', methods=['GET', 'POST'])
def login():
    form = None
    if form.validate_on_submit():
        login_user()
    return render_template('user/login.html', form=form)
