from ..main import db


class Product(db.Model):
    __tablename__ = 'product'

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(100), nullable=False)
    price = db.Column(db.Float, nullable=False)
    quantity = db.Column(db.Integer, nullable=False)
    category = db.Column(db.String(50), nullable=False)

    def __init__(self, description, price, quantity, category):
        self.description = description
        self.price = price
        self.quantity = quantity
        self.category = category

    def __repr__(self):
        return "<Product: {}>".format(self.description)
