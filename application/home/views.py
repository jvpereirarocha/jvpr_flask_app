from flask import Blueprint, render_template
import datetime


idx = Blueprint('index', __name__)


@idx.route('/', methods=['GET'])
def index():
    year = datetime.datetime.now().year
    return render_template('index.html', year=year)
