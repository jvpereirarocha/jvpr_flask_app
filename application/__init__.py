from flask import (
    Flask
)

from config import DevelopmentConfig, TestingConfig, ProductionConfig

from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_migrate import Migrate


login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'user.login'

db = SQLAlchemy()
migrate = Migrate()


def create_app(environment='Production'):
    app = Flask(__name__, instance_relative_config=True)

    if environment == 'Production':
        app.config.from_object(ProductionConfig)
    elif environment == 'Development':
        app.config.from_object(DevelopmentConfig)
    elif environment == 'Testing':
        app.config.from_object(TestingConfig)
    else:
        raise ValueError('Invalid environment name')

    db.init_app(app)
    login_manager.init_app(app)
    migrate.init_app(app, db.init_app(app))
    register_routes(app)

    return app


def register_routes(app):
    from .contact.views import ctc
    app.register_blueprint(ctc)
    from .products.views import prd
    app.register_blueprint(prd)
    from .users.views import usr
    app.register_blueprint(usr)
    from .home.views import idx
    app.register_blueprint(idx)
    app.add_url_rule('/', endpoint='index')
