import os
# from decouple import config
# from dj_database_url import parse as db_url
from dotenv import load_dotenv

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

load_dotenv(verbose=True)


class BaseConfig:
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.getenv('SECRET_KEY')
    DEBUG = True
    TESTING = False


class ProductionConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = os.getenv('DB_DEVELOPMENT_URI')
    DEBUG = False


class DevelopmentConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = os.getenv('DB_DEVELOPMENT_URI')


class TestingConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = os.getenv('DB_TESTING_URI')
    TESTING = True
